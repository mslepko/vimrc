syntax on
colo candy

set showtabline=2
set number
set smartindent
set autoindent
set smarttab
set nobackup
set noswapfile
set cursorline
set title


set ic
set incsearch
set hlsearch
set smartcase

set nocompatible
set expandtab
set tabstop=4
set softtabstop=4
set ttyfast
set showcmd
set showmode
set wildmenu

let php_sql_query=1
